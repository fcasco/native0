import { Page } from "tns-core-modules/ui/page";

import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { User } from "../shared/user/user.model";
import { UserService } from "../shared/user/user.service";


@Component({
    providers: [UserService],
    selector: "gr-login",
    styleUrls: ["app/login/login.component.css"],
    templateUrl: "app/login/login.component.html"
})

export class LoginComponent implements OnInit {
    user: User;
    isLoggingIn = true;

    constructor(private router: Router, private userService: UserService, private page: Page) {
        this.user = new User();
        this.user.email = "nnnn@grr.la";
        this.user.password = "nnnn";
    }

    ngOnInit() {
        this.page.actionBarHidden = true;
    }

    login() {
        this.userService.login(this.user)
            .subscribe(
                () => this.router.navigate(["/list"]),
                (error) => alert("Login failed")
            );
    }

    signUp() {
        this.userService.register(this.user)
        .subscribe(
            () => {
                alert("You are in now");
                this.toggleDisplay();
            },
            () => alert("Please try again tomorrow")
        );
    }

    submit() {
        if (this.isLoggingIn) {
            this.login();
        } else {
            this.signUp();
        }
    }

    toggleDisplay() {
        this.isLoggingIn = !this.isLoggingIn;
    }
}
